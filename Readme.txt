The ssh key is in:
	C:\Users\Tim\.ssh
The public key is:
	id_rsa.pub

Check that the ssh key is used (from git bash):
-----------------------------------------------
eval "$(ssh-agent -s)"
ssh-add -l -E md5

To test that the ssh key is working:
------------------------------------
ssh -T git@gitlab.com


Problems with ssh
-----------------
1/ The public key must be copied as one line only
	- if there is a return at the end of the line, then it doesn't work!!!
